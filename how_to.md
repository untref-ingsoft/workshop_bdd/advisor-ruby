# How To


Here are some useful code snippets

````ruby
# consuming the weather api at http://api.openweathermap.org/data/2.5/onecall
require 'httpclient'
require 'json'

client = HTTPClient.new
query = "#{@url}?lat=#{@lat}&lon=#{@lon}&APPID=#{@api_key}"
response = client.get query
data = JSON.parse response.body
temperature = data['current']['temp']

````


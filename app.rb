require 'sinatra'
require 'json'
Dir["./model/*.rb"].each {|file| require file }

set :bind, '0.0.0.0'
set :port, '8080'

VERSION='0.0.1-ruby'

get '/' do
  content_type :json
  foo = Foo.new
  { :version => VERSION, :foo_say => foo.say }.to_json
end
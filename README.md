Advisor App
===========

This project is based on Ruby.

* Install dependencies: bundle install
* Build & Test: rake
* Run the app: ruby app.rb

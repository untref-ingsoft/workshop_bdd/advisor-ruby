require 'spec_helper'

describe Foo do

  it 'should say foo' do
    foo = Foo.new
    expect(foo.say).to eq 'hello'
  end

end
